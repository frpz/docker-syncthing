FROM alpine:latest

MAINTAINER frpz <francis@frpz.fr>

RUN apk add --no-cache --update syncthing git bash
EXPOSE 80

